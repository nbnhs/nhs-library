# NHS Function Library

I was elected Secretary of the National Honor Society in May-ish 2018. I almost immedietally automated my job by creating the DoRoll function under the attendance folder. I further automated my job by automatically sending out emails based on these results. Later, I would automate email-sending and requirement-reductions for our mandatory community service.

In other words, I do absolutely nothing as secretary except act like I do everything. Here is living proof.

## Dependencies

* Node.js
* NPM
* Clasp (`npm i -g @google/clasp`)

## Setup

[Visual Studio Code](https://code.visualstudio.com/) is the recommended editor, with features including:

* TypeScript and JavaScript natively supported
* GitLab workflow extension for easier management
* Ability to integrate shell commands into the GUI via Tasks

*This setup assumes you have two sheet documents, one for attendance and one for service.*

1. Once you have clasp installed, use `clasp login` to login with a Google account that has editor access to the sheet you'd like to use.
2. Open a copy of each sheet you'd like to use (for testing/stating purposes). Click Tools -> Script Editor. Click File -> Project properties. Find the script ID for each and copy them.
3. Create a new file in the attendance and service folders called `.clasp.json`. Fill it with `{"scriptId":"YOUR_ID_HERE"}`, replacing `YOUR_ID_HERE` with the script ID for the right sheet.
4. Push your code to the testing sheet with either `clasp push` or the VSCode task `push attendance/service test`.

### Deploy to Production

In a GitLab repository, specify the following protected variables under Settings -> CI/CD:

* access_token
* expiry_date
* refresh_token

The values for these can be found in `.clasprc.json` under your home directory.

Also: make your pipelines private under that same page (in General) so your tokens aren't exposed and complete access to your account given to random strangers on the Internet.

Now, go to your production sheets, go under Tools -> Script Editor -> File -> Project properties, and get your script IDs. Put them in variables named `attendance_script_id` and `service_script_id`.

Now, whenever you push to master, GitLab CI/CD will push your production code to your production sheet. No other branch will trigger the deployment.
