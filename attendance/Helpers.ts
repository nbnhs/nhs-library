import { CURRENT_COLUMN } from "./current_column";
import { Member } from "./Member";

export function mark(as, row, col) {
    const sheet = SpreadsheetApp.getActiveSpreadsheet().getActiveSheet();
    const cell = sheet.getRange(row, col);
    cell.setValue(as);
}

/**
 * Returns any numbers in the provided string.
 * @param str String containing potential number.
 */
export function findNum(str: string): number {
    for (let i = 0; i < str.length; i++) {
      const test = parseInt(str.charAt(i), 10);
      if (test) {
        // true if not NaN
        return test;
      }
    }
    return NaN;
  }

  /**
   * Finds the row number corresponding to the provided ID number.
   * @param id A student ID number.
   */
export function findID(id: string): number {
    const sheet = SpreadsheetApp.getActiveSpreadsheet().getActiveSheet();
    const data = sheet.getDataRange().getDisplayValues();
    for (let i = 0; i < data.length; i++) {
      if (data[i][0] === id) {
        // 0 = first
        return i + 1; // returns row
      }
    }
    return null;
  }

  /**
   * Returns a tuple of string arrays, each array containing emails of people who have missed 1, 2, or 3 meetings.
   */
export function getEmailLists(): [string[], string[], string[]] {
    const sheet = SpreadsheetApp.getActiveSpreadsheet().getActiveSheet();
    const rows = sheet.getDataRange().getNumRows();

    const warning = [];
    const letter = [];
    const byeBye = [];

    for (let i = 1; i <= rows; i++) {
      const member = new Member(i);
      if (member.absences === 1 && member.wasAbsent) {
        warning.push(member.email);
      } else if (member.absences === 2 && member.wasAbsent) {
        letter.push(member.email);
      } else if (member.absences > 2 && member.wasAbsent) {
        byeBye.push(member.email);
      }
    }

    return [warning, letter, byeBye];

}

  /**
   * Returns the last meeting date, American style.
   * @example
   * // 11/8
   */
export function getLastMeetingDate(): string {
    const row = 1; const column = Member.sheet.getDataRange().getNumColumns() - CURRENT_COLUMN();

    return Member.sheet.getRange(row, column).getDisplayValue();
}
