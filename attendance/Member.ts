import { CURRENT_COLUMN } from "./current_column";
import { findNum } from "./Helpers";

export class Member {
    public static sheet = SpreadsheetApp.getActiveSpreadsheet().getActiveSheet();

    constructor(public row: number) {
        // Check to see if row exceeds max number of rows
        const sheet = SpreadsheetApp.getActiveSpreadsheet().getActiveSheet();
        if (row > sheet.getDataRange().getNumRows()) {
            Logger.log("A person was created with row " + String(row) + " exceeding max number of rows.");
            throw new Error("A person was created with row " + String(row) + " exceeding max number of rows.");
        }
    }

    private nameTuple(): [Object, Object] {
        const firstNameCol = 3;
        const lastNameCol = 2;

        return [Member.sheet.getRange(this.row, firstNameCol).getValue(), Member.sheet.getRange(this.row, lastNameCol).getValue()];
    }

    get name(): string {
        const arr = this.nameTuple();
        return arr[0] + " " + arr[1];
    }
    get email(): string {
        const arr = this.nameTuple();
        return arr[0] + "." + arr[1] + "@mynbps.org";
    }
    get absences(): number {
        let prior = 0;
        const numCols = Member.sheet.getDataRange().getNumColumns();

        for (let i = 1; i <= numCols; i++) {
          const current = Member.sheet.getRange(this.row, i);
          const str = "" + current.getDisplayValue();
          if (str.indexOf("Absent") !== -1 || str.indexOf("absent") !== -1) {
            const count = findNum(str);
            if (count) {
              prior = count;
            }
          }
        }
        return prior;
      }
      get wasAbsent(): boolean {
          const lastMeetingCell = Member.sheet.getRange(this.row, Member.sheet.getDataRange().getNumColumns() - CURRENT_COLUMN());

          const str = "" + lastMeetingCell.getDisplayValue();
          return str.indexOf("Absent") !== -1 || str.indexOf("absent") !== -1;
      }

}
