/** @OnlyCurrentDoc */

// camelCase indicates back-end function; CapsLikeThis indicate front-end/usability.

import { CURRENT_COLUMN } from "./current_column";
import { findID, getEmailLists, getLastMeetingDate, mark } from "./Helpers";
import { Member } from "./Member";

/**
 * Place a list of IDs in the last column, and this function will mark all corresponding people present (everyone else absent).
 */
function DoRoll() {
  const col = Member.sheet.getLastColumn();
  const rows = Member.sheet.getDataRange().getNumRows();

  for (let i = 1; i <= rows; i++) {
    const currentID = Member.sheet.getRange(i, col);
    const personRow = findID("" + currentID.getDisplayValue());
    if (personRow) {
      mark("Present", personRow, col - CURRENT_COLUMN());
    }
  }
  for (let i = 1; i <= rows; i++) {
    const filled = Member.sheet.getRange(i, col - CURRENT_COLUMN()).getDisplayValue();
    if (!filled) {
      const member = new Member(i);
      mark("Absent - " + (member.absences + 1), i, col - CURRENT_COLUMN());
    }
  }
}

// NOTE: MailApp.sendEmail() functions are commented out to prevent misuse. Uncomment to enable functionality.
function SendEmails() {
  Logger.clear();
  const emails = getEmailLists();

  let subject = "NHS 1st Meeting Absence - " + getLastMeetingDate();
  const warnings = emails[0];
  const messageEnd = `

———

The following is the criteria for an excused absence:

- Absent from school for entire day
- Dismissed early
- Participated in school-sponsored competition/performance/event
- Tech week

If you met any of these criteria, please reply to this email so that I can fix your attendance record.

Sincerely,

Milo Gilad
NHS Secretary`;

  let message = `To Whom It May Concern,

You were marked as absent (unexcused) for our National Honor Society meeting on ${getLastMeetingDate()}. You can miss a grand total of 3 meetings before being expelled from the NHS.

If you believe that you’ve received this email in error, please keep reading. Otherwise, please try not to miss 2 more meetings!`;
  message += messageEnd;
  Logger.log(message);

  for (const element of warnings) {
    Logger.log("Sent warning to: " + element);
    // MailApp.sendEmail(element, subject, message);
  }

  subject = "NHS 2nd Meeting Absence - " + getLastMeetingDate();
  const letters = emails[1];

  message = `To Whom It May Concern,

You were marked as absent (unexcused) for our National Honor Society meeting on ${getLastMeetingDate()}. This is your 2nd absence. Missing another meeting will result in your expulsion from the NHS.

If you believe that you’ve received this email in error, please keep reading.`;
  message += messageEnd;

  Logger.log(message);

  for (const element of letters) {
    Logger.log("Sent letter (not actually a letter) to: " + element);
    // MailApp.sendEmail(element, subject, message);
  }

  for (const element of emails[2]) {
    Logger.log("Mans be gone: " + element);
  }

}
