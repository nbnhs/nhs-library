import { getLastMeetingDate } from "./Helpers";
import { Member } from "./Member";

function test() {
    const member = new Member(4);
    Logger.log(member.name);
    Logger.log(member.email);
    Logger.log(member.absences);
    Logger.log(member.wasAbsent);
    Logger.log(getLastMeetingDate());
}
