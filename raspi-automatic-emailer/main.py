#! /usr/bin/env python3

# Run me daily via chron!
# Expecting the following in pwd: client_id.json, .clasp.json

from subprocess import call
import pickle
from datetime import datetime

def daysBetween(d1, d2):
    print(abs((d2 - d1).days))
    return abs((d2 - d1).days)

def today():
    return datetime.now()

def lastMeeting():
    meetings = []
    with open('meetingdates.pkl', 'rb') as f:
        meetings = pickle.load(f)    
    return meetings[0]

def cycleMeetingList():
    meetings = []
    with open('meetingdates.pkl', 'rb') as f:
        meetings = pickle.load(f)

    lastMeet = meetings[0]
    meetings.remove(lastMeet)

    with open('meetingdates.pkl', 'wb') as f:
        pickle.dump(meetings, f)

def meetingLastWeek():
    return daysBetween(datetime.now(), lastMeeting()) == 7

def sendEmails():
    pass
    # call(["clasp", "login", "--creds", "client_id.json"])
    # call(["clasp", "run", "SendEmails"])

def main():
    if meetingLastWeek():
        sendEmails()
        cycleMeetingList()
        print("Sent emails")

main()