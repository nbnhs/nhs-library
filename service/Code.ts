import { runFuncOnAllMembers, findName, markForLatestEvent } from "./Helpers";
import { Member } from "./Member";

/**
 * Sends custom-tailored emails to each member informing them of their current service status.
 *
 * - Gets their service event count
 * - If >= 2: reduce requirement by one and notify them
 * - Else: Don't reduce but still notify
 * - Lists the events they participated in
 * - Puts all that in an email and sends it too
 */
function SendCheckpointEmails() {
  Logger.clear();

  runFuncOnAllMembers((member: Member) => {
    let message = "Dear " + member.name + ",\n\n";

    if (member.service >= 2) {
      // member.lowerRequirement(1);
      message += `Thank you for pitching in to help our community!

You helped in ${String(
        member.service,
      )} NHS service events before the end of Trimester 1, and as such your overall event requirement has been reduced by one.

If you have any questions, feel free to email milo.gilad@mynbps.org for anything.`;
    } else {
      message += `You completed ${String(
        member.service,
      )} service event(s) before the end of Trimester and as such are ineligible for a reduction in the total number of events you are required to complete.

If you believe to have received this message in error, please reply or send a message to milo.gilad@mynbps.org at once.

Otherwise, please try to get your service events done before the end of the school year. Failure to do so will result in the termination of your membership.

If you have any questions, please let me know.`;
    }

    if (member.service !== 0) {
      message += "\n\nEvents you participated in: \n\n";
      for (const participatedEvent of member.eventsParticipated) {
        message += `- ${participatedEvent}\n`;
      }
      message += "\n";
    }

    message += `You have ${String(
      member.eventsLeft,
    )} events left before your yearly requirement is met.

Yours Truly,

Milo Gilad
NHS Secretary`;

    // MailApp.sendEmail(member.email, subject, message);
    Logger.log("Sent email to " + member.name);
    Logger.log(message);
  });
}

function SendFinalWarning() {
  Logger.clear();

  runFuncOnAllMembers((member: Member) => {
    if (!member.requirementMet) {
      const subject = "NHS Service Event Reminder";

      const message = `Dear ${member.name},

This is a reminder that you have ${String(
        member.eventsLeft,
      )} event(s) left before your service requirement is complete.

Failure to complete this requirement before the end of the school year will result in your expulsion from the National Honor Society.

If you have any questions, feel free to reply to this email.

Sincerely,

Milo Gilad
NHS Secretary`;

      Logger.log("Sent to: " + member.name);
      Logger.log(message);
      // MailApp.sendEmail(member.email, subject, message);
    }
  });
}

function InputMembersForLatestEvent() {
  Logger.clear();

  const col = Member.sheet.getLastColumn();
  const rows = Member.sheet.getDataRange().getNumRows();

  for (let i = 1; i <= rows; i++) {
    const currentName = Member.sheet.getRange(i, col);
    const personRow = findName("" + currentName.getDisplayValue());
    if (personRow) {
      markForLatestEvent(personRow);
    }
  }
}
