import { CURRENT_COLUMN } from "./current_column";
import { Member } from "./Member";

function memberList(): Member[] {
  const members = [];
  const rows = Member.sheet.getDataRange().getNumRows();

  for (let i = 2; i <= rows; i++) {
    members.push(new Member(i));
  }
  return members;
}

export function runFuncOnAllMembers(func) {
  for (const member of memberList()) {
    func(member);
  }
}

// Finds a name and returns the row
export function findName(n: string): number | null {
  const data = Member.sheet.getDataRange().getDisplayValues();
  for (let i = 0; i < data.length; i++) {
    // Last names are in col B, firsts are in C (0 = A)
    const first = n.split(" ")[0];
    const last = n.split(" ")[1];
    const lastNameMatch = data[i][1] === last;
    const firstNameMatch = data[i][2] === first;

    if (lastNameMatch && firstNameMatch) {
      return i + 1; // returns row
    } else if (!firstNameMatch && lastNameMatch) {
      Logger.log(`Could not confirm first name of ${first} ${last}`);
      return null;
    } else if (firstNameMatch && !lastNameMatch) {
      Logger.log(`Could not confirm last name of ${first} ${last}`);
      return null;
    }
  }
  return null;
}

export function markForLatestEvent(row: number): void {
  const cell = Member.sheet.getRange(row, Member.sheet.getLastColumn() - CURRENT_COLUMN());
  cell.setValue("1");
}
