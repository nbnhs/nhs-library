export class Member {

    get name(): string {
        const arr = this.nameTuple();
        return arr[0] + " " + arr[1];
    }
    get email(): string {
        const arr = this.nameTuple();
        return arr[0] + "." + arr[1] + "@mynbps.org";
    }
    get service(): Object {
        return Member.sheet.getRange(this.row, Member.sheet.getLastColumn() - 2).getValue().valueOf();
    }
    get eventsLeft(): number {
        const reqCell = Member.sheet.getRange(this.row, Member.sheet.getLastColumn() - 1);
        return reqCell.getValue().valueOf() - this.service;
    }
    get eventsParticipated(): string[] {
        const result = [];
        // i is initialized to 5 because the 4 preceding rows are all member information.
        for (let i = 5; i <= Member.sheet.getDataRange().getNumColumns() - 3; i++) {
            if (Member.sheet.getRange(this.row, i).getValue().valueOf() === 1) {
              result.push(Member.sheet.getRange(1, i).getValue());
            }
        }
        return result;
    }
    get requirementMet(): boolean {
        return this.eventsLeft <= 0;
    }
    public static sheet = SpreadsheetApp.getActiveSpreadsheet().getActiveSheet();

    constructor(public row: number) {
        // Check to see if row exceeds max number of rows
        const sheet = SpreadsheetApp.getActiveSpreadsheet().getActiveSheet();
        if (row > sheet.getDataRange().getNumRows()) {
            Logger.log("A person was created with row " + String(row) + " exceeding max number of rows.");
            throw new Error("A person was created with row " + String(row) + " exceeding max number of rows.");
        }
    }

    public lowerRequirement(by: number) {
        const reqCell = Member.sheet.getRange(this.row, Member.sheet.getLastColumn() - 1);
        reqCell.setValue(reqCell.getValue().valueOf() - by); // This is only an error in typescript; not GAS
    }

    private nameTuple(): [Object, Object] {
        const firstNameCol = 3;
        const lastNameCol = 2;

        return [Member.sheet.getRange(this.row, firstNameCol).getValue(), Member.sheet.getRange(this.row, lastNameCol).getValue()];
    }

}
