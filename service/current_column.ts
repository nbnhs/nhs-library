// This file specifies the column for the latest meeting, from last to first, with 0 representing the last column.

export function CURRENT_COLUMN(): number {
    return 4;
}
